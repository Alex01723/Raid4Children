<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Raid4Children</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Add custom CSS here -->
    <link href="css/slidefolio.css" rel="stylesheet">
	<!-- Font Awesome -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <!-- Bootstrap social -->
    <link href="css/bootstrap-social.css" rel="stylesheet">
      <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
      <link href="css/bootstrap-dialog.min.css" rel="stylesheet">
  </head>
  <body>
    <!-- Header Area -->
    <div id="top" class="header">
      <div class="vert-text container-fluid"  >
          <div class="container-fluid" style="margin-top: 4vh;">
              <div class="hidden-xs ">
                  <h1>RAID 4 CHILDREN</h1>
                  <h3>EQUIPAGE #0755</h3>
                  <h3>PICOT Alexis – RAVAIL Thomas</h3>
              </div>
              <div class="visible-xs " >
                  <h1>RAID 4 CHILDREN</h1>
                  <h3>EQUIPAGE #0755</h3>
                     <h3  > PICOT Alexis<br/>
                      RAVAIL Thomas</h3>
              </div>
          </div>
	    <img class=".img-responsive" alt="Company Logo" style="width: 80%; max-width: 500px; margin-bottom: 2vh" src="./img/logo.png"/>

		 <ul class="list-inline">
              <li>
                  <a class="btn btn-social-icon btn-facebook">
                      <span class="fa fa-facebook"></span>
                  </a>
              </li> <li>
                  <a class="btn btn-social-icon btn-twitter">
                      <span class="fa fa-twitter"></span>
                  </a>
              </li> <li>
                  <a class="btn btn-social-icon btn-instagram">
                      <span class="fa fa-instagram"></span>
                  </a>
              </li>

            </ul>	
			<br>
			<a href="#about" class="btn btn-top">Learn More</a>
      </div>
    </div>
    <!-- /Header Area -->
	  <div id="nav">
    <!-- Navigation -->
    <nav class="navbar navbar-new" role="navigation">
   <div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobilemenu">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
	<a href="#" class="navbar-brand">RAID 4 CHILDREN <br/> 4L TROPHY 2018</a>
  </div>
  <div class="collapse navbar-collapse" id="mobilemenu">

	  <ul class="nav navbar-nav navbar-right text-center">
	    <li><a href="#top"><i class="service-icon fa fa-home"></i>&nbsp;Home</a></li>
        <li><a href="#about"><i class="service-icon fa fa-info"></i>&nbsp;About</a></li>
        <li><a href="#services"><i class="service-icon fa fa-laptop"></i>&nbsp;Services</a></li>
        <li><a href="#portfolio"><i class="service-icon fa fa-camera"></i>&nbsp;Portfolio</a></li>
        <li><a href="#contact"><i class="service-icon fa fa-envelope"></i>&nbsp;Contact</a></li>
    </ul>
  </div><!-- /.navbar-collapse -->
  </div>
</nav>
    <!-- /Navigation -->
</div>	
    <!-- About -->
    <div id="about" class="about_us">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-center">
            <h2>Le projet</h2>

            <p class="lead">Lorem ipsum dolor sit amet, ei essent delenit sit, adipisci salutatus has eu. Quis tamquam cu nam. Sed esse deleniti et, ex rebum quaestio his. Audiam deseruisse sed cu, vix ex possim causae omittantur.</p>
          </div>
        </div>
	  </div>
    </div>
    <!-- /About -->
    <!-- Services -->
    <div id="services" class="services">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 text-center">
            <h2>Devenir partenaire</h2>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="service-item">
              <i class="service-icon fa fa-camera-retro fa-3x"></i>
              <h3>Don mécenat</h3>
                <p>Vous vous sentez engagés par notre action et souhaitez nous soutenir en effectuant un don ? <a href="#contact"> Contactez-nous !</a> <br/>
              (Les dons sont éligibles aux crédits d'impôts et vous figurerez parmis nos partenaires)</p>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="service-item">
			<i class="service-icon fa fa-camera fa-3x"></i>
              <h3>Parrainage</h3>
              <p>Vous souhaitez mener une campagne marketing ? Decouvrez nos options pour obtenir une visibilité sur notre voiture</p>
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Voir les options</button>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="service-item">
              <i class="service-icon fa fa-globe fa-3x"></i>
              <h3>Don en nature</h3>
              <p>Vous préferez apporter un soutien matériel en fournissant par exemple des fournitures scolaire, des pièces de rechange ou encore des réparations ? <a href="#contact"> Contactez-nous !</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- /Services -->

    <!-- Portfolio -->
    <div id="portfolio" class="portfolio">
    <div class="container">
    <div class="row push50">
          <div class="col-md-4 col-md-offset-4 text-center">
            <h2>Nos partenaires</h2>
			<h3>
	</h3>
            <hr>
          </div>
        </div>
		
		<div class="row">
		
		<div class="gallery">

            <ul id="Grid" class="gcontainer">
            <?php
            $filter = function($filename){
                return '<li class="col-md-4 col-sm-4 col-xs-12 mix bw portraits" data-cat="graphics">              
                <div class="horizontal mix-cover" style="background-image: url('.str_replace(" ","%20",$filename).');
                background-size: contain;
                
                padding-top: 100%;
                margin: auto;
                background-position: center;
                background-repeat: no-repeat;"
                />
      		    <span class="overlay"><span class="valign"></span><span class="title">'.str_replace("./img/sponsors/","",$filename).'</span></span>  
      		      
      		                
      		  </li>'; };
            $spannedTags = array_map($filter, glob("./img/sponsors/*"));
            echo implode("\n",$spannedTags);
            ?>

            </ul>
        </div>
			  

		</div>	
      </div>
		</div>
      </div>
    <!-- /Portfolio -->
    <!-- Contact -->
    <div id="contact">
      <div class="container">
        <div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">
            <h2>Nous contacter</h2>
			<hr>
          </div>
          <div class="col-md-8 col-md-offset-2">
		  <!-- contact form starts -->
            <form action="contact" id="contact-form" class="form-horizontal">
			<fieldset>
						    <div class="form-group">
						      <label class="col-sm-4 control-label" for="name">Nom</label>
						      <div class="col-sm-8">
						        <input type="text"  placeholder="Nom" class="form-control" name="name" id="name">
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="col-sm-4 control-label" for="email">Adresse Electronique</label>
						      <div class="col-sm-8">
						        <input type="text" placeholder="Adresse" class="form-control" name="email" id="email">
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="col-sm-4 control-label" for="message">Votre message</label>
						      <div class="col-sm-8">
						        <textarea placeholder="Message" class="form-control" name="message" id="message" rows="3"></textarea>
						      </div>
						    </div>
	              <div class="col-sm-offset-4 col-sm-8">
			            <button type="submit" class="btn btn-success">Submit</button>
	    			      <button type="reset" class="btn btn-primary">Cancel</button>
	        			</div>
						</fieldset>
						</form>
				
				<!-- contact form ends -->		
          </div>
        </div>
      </div>
    </div>
    <!-- /Contact -->




    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <? require("modal.php");?>
    </div>
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3 text-center">
           <h2>Thank You</h2>
           <em>Copyright &copy; Company 2013</em>
          </div>
        </div>
      </div>


    </footer>
    <!-- /Footer -->
    <!-- Bootstrap core JavaScript -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
	<script src="js/jquery-scrolltofixed-min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.vegas.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/script.js"></script>
    <!-- Slideshow Background  -->
	<script>
        $.vegas('slideshow', {
          delay:5000,
          backgrounds:[
              <?php
                $filter = function($filename){ return "{ src: '$filename', fade:2000}"; };
                $spannedTags = array_map($filter, glob("./img/backgrounds/*"));
                echo implode(",\n",$spannedTags);
              ?>
          ]
        })('overlay', {
        src:'./img/overlay.png'
        });

	</script>
<!-- /Slideshow Background -->

<!-- Mixitup : Grid -->
    <script>
		$(function(){
    $('#Grid').mixitup();
      });
    </script>
<!-- /Mixitup : Grid -->	

    <!-- Custom JavaScript for Smooth Scrolling - Put in a custom JavaScript file to clean this up -->
    <script>
      $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });
    </script>
<!-- Navbar -->
<script type="text/javascript">
$(document).ready(function() {
        $('#nav').scrollToFixed();
  });
    </script>
<!-- /Navbar-->


	
  </body>

</html>